﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    [SerializeField] private Plane plane;

    // TODO Refaire via Touch
    void LateUpdate()
    {
        if (Input.GetMouseButton(0))
        {
            plane.MoveUp();
        }

        if (Input.GetMouseButtonDown(0))
        {
            plane.StartThrusterPS();
        }

        if (Input.GetMouseButtonUp(0))
        {
            plane.StopThrusterPS();
        }
    }
}
