﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIManager : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI timerTMP;
    [SerializeField] TextMeshProUGUI speedTMP;
    [SerializeField] TextMeshProUGUI fuelTMP;

    [SerializeField] GameObject RestartPanel;
    [SerializeField] TextMeshProUGUI TimeTMP;
    [SerializeField] TextMeshProUGUI BestTMP;

    [SerializeField] GameObject SpeedIndicator;
    [SerializeField] GameObject FuelIndicator;

    private float speedAxisValue;
    private float fuelAxisValue;

    // Update is called once per frame
    void Update()
    {
        fuelAxisValue = Mathf.LerpAngle(0, 90, GameManager.Instance.GetFuelPercentage());
        FuelIndicator.transform.rotation = Quaternion.Euler(FuelIndicator.transform.rotation.x, FuelIndicator.transform.rotation.y, fuelAxisValue);

        // TODO Calculer speed par rapport a valeur max pour Lerp l'aiguille
        SpeedIndicator.transform.rotation = Quaternion.Euler(SpeedIndicator.transform.rotation.x, SpeedIndicator.transform.rotation.y, speedAxisValue );

        timerTMP.text = GameManager.Instance._timer.ToString("F1") + "s" ;
        speedTMP.text = (GameManager.Instance.GetWorldRotationSpeed().z * 2000).ToString("F0") + "Km/h";
        fuelTMP.text = "FUEL :" + (100 - (GameManager.Instance.GetFuelPercentage() * 100)).ToString("F0") + "%";

        if (GameManager.Instance._isgameOver)
        {
            ShowRestartPanel();
            GameManager.Instance.StopGame();
        }
    }

    void ShowRestartPanel()
    {
        TimeTMP.text = "LAST " + GameManager.Instance._timer.ToString("F1");
        BestTMP.text = "BEST " + PlayerPrefs.GetFloat("BestTime").ToString("F1");

        RestartPanel.SetActive(true);
    }
}
