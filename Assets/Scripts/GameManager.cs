﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [Header("Plane variables")]
    [SerializeField] private float _planeVerticalSpeed;
    [SerializeField] private float _startSpeed;
    [SerializeField] private float _maxSpeed;
    [SerializeField] private float _timeForMax;
    [SerializeField] private float _gravity = 5;
    [SerializeField] private float _maxFlightTime;

    [Header("Bonus variables")]
    [SerializeField] public float chanceToSpawnBonus = .5f;
    [SerializeField] private float _bonusValue = .1f;

    public bool _isthusting { get; set; }
    public bool _isgameOver { get; set; }
    public float _timer { get; set; }

    private bool _isGameLaunch = true;
    private float _thrust = 0;
    public float _fuel { get; private set; }


    private static GameManager _instance;


    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }

    void Awake()
    {  
        _instance = this;
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void StopGame()
    {
        // TODO Refaire le BestTime sans Palyerprefs
        if (PlayerPrefs.HasKey("BestTime") || _timer > PlayerPrefs.GetFloat("BestTime"))
        {
            PlayerPrefs.SetFloat("BestTime", _timer);
        }

        _isGameLaunch = false;
        _isgameOver = true;
    }

    public float GetMaxSpeed()
    {
        return _maxSpeed;
    }

    public Vector3 GetPlaneVerticalVelocity()
    {
        Vector3 planeVerticalVelocity = new Vector3(0, _planeVerticalSpeed, 0);
        return planeVerticalVelocity;
    }

    public Vector3 GetWorldRotationSpeed()
    {
        float speed = Mathf.Lerp(_startSpeed, _maxSpeed, _thrust / _timeForMax);
        Vector3 worldRotationSpeed = new Vector3(0, 0, speed);

        return worldRotationSpeed;
    }

    public float GetTimer()
    {
        return _thrust;
    }

    public float GetFuelPercentage()
    {
        _fuel = _thrust / _maxFlightTime;

        return _fuel;
    }

    public void AddBonusTime()
    {
        _thrust -= _bonusValue;
    }

    public Vector3 GetGravity()
    {
        return new Vector3 (0, -_gravity, 0);
    }

    public void Update()
    {
        if(!_isGameLaunch) { return; }

        if (_isthusting)
        {
            _thrust += Time.deltaTime;
        }

        _timer += Time.deltaTime;
    }
}
