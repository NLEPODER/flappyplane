﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusSpawner : MonoBehaviour
{
    [SerializeField] GameObject BonusPrefab;

    private GameObject _thisBonus;

    // Start is called before the first frame update
    void Start()
    {
        _thisBonus = Instantiate(BonusPrefab, transform.position, Quaternion.identity, transform);
        _thisBonus.SetActive(false);

        SpawnBonus();
    }

    private void SpawnBonus()
    {
        if (Random.value < GameManager.Instance.chanceToSpawnBonus)
        {
            _thisBonus.SetActive(true);
        }
    }

    public void TakeBonus()
    {
        _thisBonus.SetActive(false);
        Invoke("SpawnBonus", 2);
    }
}
