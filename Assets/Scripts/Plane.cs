﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plane : MonoBehaviour
{
    private Rigidbody _rb;
    private Animator _animator;

    [SerializeField] ParticleSystem ThrusterPS;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _animator = GetComponentInChildren<Animator>();

        StopThrusterPS();
    }

    public void MoveUp()
    {
        if (GameManager.Instance._fuel > 1) { StopThrusterPS(); return; }

        _rb.AddForce(GameManager.Instance.GetPlaneVerticalVelocity() * Time.deltaTime, ForceMode.Force);
    }

    public void StartThrusterPS()
    {
        ThrusterPS.Play();
        GameManager.Instance._isthusting = true;
    }

    public void StopThrusterPS()
    {
        ThrusterPS.Stop();
        GameManager.Instance._isthusting = false;
    }

    void Update()
    {
        _rb.AddForce(GameManager.Instance.GetGravity() * Time.deltaTime, ForceMode.Force);
        Vector3 fakeVelocity = new Vector3(transform.rotation.x + 10, _rb.velocity.y, transform.rotation.z);
        transform.rotation = Quaternion.LookRotation(fakeVelocity);
    }

    void OnTriggerEnter (Collider other)
    {
        if (other.tag == "Bonus")
        {
            _animator.SetTrigger("Bonus");
            other.GetComponentInParent<BonusSpawner>().TakeBonus();
            GameManager.Instance.AddBonusTime();
        }

        else
        {
            GameManager.Instance.StopGame();
            Debug.Log(other.gameObject.name);
        }
    }
}
