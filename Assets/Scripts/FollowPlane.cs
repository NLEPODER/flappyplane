﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlane : MonoBehaviour
{
    [SerializeField] private Transform planePos;
    [SerializeField] private float cameraPlaneOffset = 10;

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(planePos.position.x - cameraPlaneOffset, planePos.position.y, 0);
    }
}
