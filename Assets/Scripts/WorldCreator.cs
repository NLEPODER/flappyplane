﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldCreator : MonoBehaviour
{
    [SerializeField] int numPoints;
    [SerializeField] GameObject[] Obstacles;
    [SerializeField] float radiusX = 200;
    [SerializeField] float radiusY = 200;

    // Start is called before the first frame update
    void Start()
    {
        CreateWorld();
    }

    void CreateWorld()
    {
        for (var pointNum = 0; pointNum < numPoints; pointNum++)
        {
            double i = (pointNum * 1.0) / numPoints;

            float angle = (float)(i * Mathf.PI * 2);

            float x = Mathf.Sin(angle) * radiusX;
            float y = Mathf.Cos(angle) * radiusY;

            Vector3 pos = new Vector3(x, y) + transform.position;

            Vector3 normal = (pos - transform.position).normalized;
            Vector3 tangent = Vector3.Cross(normal, pos);
            
            GameObject obstacle = Instantiate(Obstacles[Random.Range(0, Obstacles.Length)], pos, Quaternion.LookRotation(tangent, normal), gameObject.transform);
        }
    }
}